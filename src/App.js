import React from 'react';
import Container from '@material-ui/core/Container';
import Table from './Table';
import Navi from './Navi';
import './App.css';

function App() {
  return (
    <>
    <Navi />
    <Container maxWidth="xl">
        <Table />
    </Container>
    </>
  );
}

export default App;
